# Lab 3: Proactive Security and Automated Risk Management with Red Hat Insights

<!-- TOC -->

- [Lab 3: Proactive Security and Automated Risk Management with Red Hat Insights](#lab-3%C2%A0proactive-security-and-automated-risk-management-with-red-hat-insights)
  - [Goal of Lab](#goal-of-lab)
  - [Introduction](#introduction)
    - [Installing the Insights client](#installing-the-insights-client)
    - [Fixing critical issues](#fixing-critical-issues)

<!-- /TOC -->

## Goal of Lab


The goal of this lab is to introduce you to the proactive security
capabilities of Red Hat Insights. This lab assumes the following:

-   You started all your VMs, per the instructions in Lab 0.
-   Did not delete any of the key items in the pre-populated Satellite 6
    configuration, which are necessary for Insights to work properly.

## Introduction


Red Hat Insights was designed to proactively evaluate the security,
performance, and stability of your Red Hat platforms by providing
prescriptive analytics of your systems. Insights helps move you from
reactive to proactive systems management, delivers actionable
intelligence, and increases visibility of infrastructure risks and the
latest security threats. Operational analytics from Insights empowers
you to prevent downtime and avoid firefighting, while also responding
faster to new risks.

In this lab, we will focus only on the specific security features of Red
Hat Insights.

Red Hat Insights recommendations are tailored for the individual system
where risk is detected. This allows you to be certain that actions
identified by Insights are validated and have a verified resolution for
each detected risk, reducing false positives you may experience from
critical risks identified by third-party security scanners. Insights
provides predictive analysis of security risk in your infrastructure
based on a constantly evolving threat feed from Red Hat.

Through analysis of Insights metadata and curated knowledge based on
over fifteen years of enterprise customer support, Red Hat is able to
identify critical security vulnerabilities, statistically frequented
risks, and known bad configurations. We scale this knowledge to our
customers with Insights reporting and alerts, allowing prediction of
what will happen on a monitored system, why it will happen, and how to
fix a problem before it can occur.

Red Hat Insights functionality is integrated into Red Hat’s Customer
Portal, Satellite, CloudForms, and Ansible Tower by Red Hat.
 Recommendations from Insights are human-readable and in most cases can
simply be copy and pasted into the terminal to resolve the issue. You
may also automate remediation of hosts in your infrastructure with
Insights generated Ansible playbooks, Red Hat Satellite integration or Ansible Tower integration.

### Installing the Insights client

Now it’s the time to install the Insights client and register your system
to Red Hat Insights.

Installing Insights is quick and easy and if your system is already registered to Satellite like it is in this lab or to subscription manager.

The Insights client can be automatically installed when you register systems to Satellite using the bootstrap script.
The Insights client can also be deployed in Satellite via an Ansible playbook.

To manually install the Insights client on host ic1-ic3 you must first connect to the host via SSH.
 
  * Note: for host ic4 we will install Insights via Satellite, so please do not manually install Insights on ic4.example.com

SSH from your jumpbox as outlined in Lab 0 to root@ic1.example.com, the password is **r3dh4t1!**

`[lab-user@workstation \$] ssh root@ic1`

To install the Insights client, issue the following command:

`[root@ic1 \~]\# yum -y install insights-client`

Near the end of the details on page you should have a line that reads similar to:

```
Updated or Installed:
  insights-client.noarch 0:3.0.6-2.el7_6 
```

With RHEL8 the Insights-client is installed by default.  
In RHEL 6 and 7 you will need to install the client.
Even with the client installed, it doesn't do anything until you register the systems to Insights.


Since we are installing into a shared lab environment where all of the systems have the exact name UUID and hostname, for the purposes of the lab we are going to generate a unique UUID prior to registering Insights.
This step is only for the lab and is not typically needed for Insights.

To generate a unique machine ID for Insights, issue the following commands:

`[root@ic1 \~]\# rm -f /etc/insights-client/machine-id`

`[root@ic1 \~]\# dbus-uuidgen --ensure=/etc/machine-id`

Now you are ready to register the hosts to Insights by running the: insights-client --register command:

```
[root@ic1 ~]# insights-client --register
Successfully registered host ic1.example.com 
Automatic scheduling for Insights has been enabled.
Starting to collect Insights data for ic1.example.com
Uploading Insights data.
Successfully uploaded report from ic1.example.com to account XXXXXXX.
```

That completes the installation and registration of Insights on ic1.
Repeat these steps with ic2 and ic3.
The getting started page for Red Hat Insights also includes links to automate this process through popular automation tools like Ansible, Puppet, and Chef.

**Let's install the Insights client on ic4 via Satellite**

Log into the Satellite UI
Point your web browser to https://sat-</GUID/>.rhpds.opentlc.com or click on your Lab GUID page that is open in your browser and open the link for your Sat Where <GUID> is your unique identifier as described in Lab 0.
Login as user **admin** with the password **r3dh4t1!** as mentioned previously in this guide.

On the left hand navigation bar select **Hosts--> All Hosts**.
Select **ic4.example.com**.  This will show you the status and information about the host.  

**Note**: You might noticed that the Status reads error.  This is expected and is shown that way since below Satellite has identified that there is security errata available that needs to be installed.

In the upper right corner there are a series of buttons.  Locate and click **Edit**.

![](images/image10.png)

This will show you information about the host.  This information is often automatically set for you based on selections like Host Group.

Click the **Ansible Roles** tab at the top.

![](images/image5.png)

**Note:** there is an Ansible Role for Red Hat Insights named: **RedHatInsights.insights-client**. Click on this role to move it as part of the Selected Items.

![](images/image3.png)

Click **Submit**. This will return you to the status page. Click the **Run Ansible Roles** button This will redirect you to the Jobs page. 

![](images/image111.png)

Running this role will take about a minute.  When completed the page will read **Success**.

![](images/image112.png)


You can return to the host page by clicking the **Host Detail** button for ic4.example.com near the bottom of the jobs page.

Click **Insights --> Inventory** to look at the list of hosts registered to Red Hat Insights.

![](images/Sat64_Insights_Inv.png)

### Fixing critical issues

Still in the Satellite UI, click on **Insights →Overview,** where you should see all your registered systems, actions
summary (highlighted by priority) as well as latest updates from RedHat.

![](images/Sat64_Insights_Overview.png)

In this lab, we will focus on the critical issues identified by Insights.
The goal is to resolve all critical issues on affected hosts.

The specific fixes are to fix the fix the **OpenSSH** vulnerable to remote password guessing attack **(CVE-2015-5600)**
issue on your client VMs without causing downtime.

#### Appliying hotfixes with Insights:

1.  From your Satellite  UI, Click **Montitor → Dashboard**. 

        ![](images/Sat64_Overview_w_Insights.png)

2.  Note that the Overview page includes at the top two widgets for Insights.
	* The Red Hat Insights Risk Summary and the Red Hat Insights Actions.
	* The Insights Risk Summary widget identifies the different risks present in the environment by the criticality of the risk, while the Red Hat Insights Actions widget lists risks by category.

**NOTE**: that the Red Hat Insights Risk Summary widget identifies that 4 systems have critical issues.  We want to address these immediately.


3.  Click **4 system(s) have critical issues** This redirects you to **Insights > Actions > Critical Risk Actions** in a single click. The Critical Risk actions page identifies the two critical issues that Insights has detected are present in the environment.
These are considered critical because Red Hat's expereince helping customers with these risks in the past has identified that:
The liklihood that these hosts will encounter the risk is high The Impact of the risk is high and the Total Risk is a combination of the liklihood and impact scores.
Since they are both high the total risk is considered high and therefore this is a critical risk.

The critical risk actions page details the rule or risk that was encountered, the liklihood, impact, and total risk as mentioned earlier, the number of systems impacted, and if there is an Ansible playbook avaialble to help resolve the risk.
Not all risks have Ansible playbooks, but many do.


![](images/Sat64_Insights_Crit_Risks.png)


4.  Select **OpenSSH** vulnerable to remote password guessing attack **CVE-2015-5600**.

Once you select the risk you can see all of the details about it.  At the top you generally have a description of the risk, a summary of the change needed to resolve the risk, and the expected risk of making the change.
In this case the OpenSSH could allow an attacker to request a large number of keyboard-interactive devices when entering a password and bypass the MaxAuthTries limit.
This issue is dependent on KbdInteractiveAuthentication and ChallengeResponseAuthentication settings in sshd_config.
to resolve, Red Hat recommends that you update your settings in sshd_config.
The risk of making this change is considered very low.

All four systems registered to Insights have this risk, so we certainly want to address this.

![](images/Sat64_Insights_OpenSSH_Overview.png)

5.  We can fix this issue right through Satellite.
Start by selecting the hosts that yu want to perform this fix on.
In a production environment you might want to first test the fix on a single system, but since this is a lab environment we will just select all of the affected hosts.
Select the checkbox in the upper left of the table listed all of the impacted systems.  This will select all of the systems.

![](images/Sat64_Insights_OpenSSH_SelectAll.png)

6.  Will all hosts selected, click the Actions button and select **Create a new Plan/Playbook** from the dropdown menu

![](images/Sat64_Insights_OpenSSH_CreatePB.png)

7.  The Plan/Playbook builder will appear on screen.
Give the plan a name, for example  **Fix Critical Issues** and click Save

![](images/Sat64_Insights_OpenSSH_NamePB.png)

8.  This **OpenSSH risk (CVE-2015-5600)** has multiple options for resolution as part of the Insights resolution playbook.
You have the option to update the openssh-server package then restart the service OR you can just update the parameters in the sshd_config file.
Either are acceptable options to mitigate this rick.
Accept the default option by clicking Save.

![](images/Sat64_Insights_OpenSSH_FixOptions.png)

9.  Review the plan that was created.
At this point you have created a plan, but not taken any actions.
You may need to open a change request ticket in your organization to get approval to make this change.
Don't leave the page, but if needed this page can be revisited at a later time by clicking Insights then planner.

Since this is a lab environment we don't need to wait for approvals, but if you recall the goal is to resolve both of the critical issues.
This plan only addresses the OpenSSH issue at the moment but you can explore around and fix other issues if you would like, don't be afraid is just a lab enviroment

![](images/Sat64_Insights_OpenSSH_Plan.png)

![](images/Sat64_Insights_Crit_Plan_perSystem.png)

14. In the plan click the Playbook tab.  This shows the information from the perspective of the actions occurring in the playbook.
Notice this view also include the risk of change as well as if the playbook requires systems to be rebooted.
At the bottom of the page (and present in all of these view) is the run playbook button.
This button is availble as of Satellite 6.4 and enables you to run playbooks generated by Insights inside of Satellite.
Click the down facing arrow to the right of the Run Playbook button and notice that you have the options to customize the playbook run
(allowing you to change any options selected when you created your plan, like what you did in step 8)
or download the playbook which will allow you to edit and run the playbook from another host or via Ansible Tower.

**Note:** Some customers have hosts that are in isolated environments where they do not have internet access.
Some of these customers have still had success using Insights by taking a system that is connected to the internet like the hosts in this lab,
then downloading the playbook and adding hosts that do not have access to the internet.
If your hosts are all built off a common base image or with a Satellite Standard Operating Environment (SOE) then it is likely that any risks
that exist in your internet facing hosts also exist in your isolated hosts.
The download playbook button will allow you to download the playbook, change the hosts line, then run the playbook using Ansible automation inside of your isolated environment.


![](images/Sat64_Insights_Crit_Plan_perPB.png)

15. Click The Run Playbook button to fix the critical issues.
This will redirect you to the job page in Satellite where you can wait for the jobs to complete.
This should take about three minutes.

![](images/Sat64_Insights_Success.png)

16. In the Satellite UI click **Monitor --> Dashboard**.
You should note that no critical issues remain.

![](images/Sat64_Insights_NoCrits.png)

Continue to the next step: [Lab 4: Introduction to Ansible Tower](../lab4-tower/index.md)
