Thanks for helping us with this project!

Please, fork this repo, clone it, make your changes locally and submit your merge request. We appreciate small merge requests so we can validate the changes. Should you have any question, please reach me on Google Chat or email: vestival@redhat.com